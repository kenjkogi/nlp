from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

sample_text = "This is an example of a sentence with stopwords that are the same as those in the Youtube tutorial"
stop_words = set(stopwords.words('english'))

words = word_tokenize(sample_text)

filtered_sentence = []

for word in words:
    if word not in stop_words:
        filtered_sentence.append(word)

print(filtered_sentence)
