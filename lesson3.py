from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

ps = PorterStemmer()

# words = ['python', 'pythoner', 'pythoning', 'pythoned', 'pythonly']
#
# for word in words:
#     print(ps.stem(word))

sentence = 'It is very important to be pythonly while you are pythoning with python. All pythoners have pythoned poorly at least once'

words2 = word_tokenize(sentence)

for word2 in words2:
    print(ps.stem(word2))
